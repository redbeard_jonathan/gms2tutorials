{
    "id": "aed06ce6-902c-4fcb-bb1a-1cafa15ee535",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_score",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "6c70b496-1acb-48c1-a7d1-95a1394c6075",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 45,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 53,
                "y": 143
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b609bfb2-2850-4a31-9a40-1dc19dfef527",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 45,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 169,
                "y": 143
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "4579eced-eef4-4b19-b3cf-ed255b2541ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 45,
                "offset": 1,
                "shift": 14,
                "w": 10,
                "x": 41,
                "y": 143
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "00bede21-1a81-4509-ac9f-9ee6bf5c0f34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 45,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a56ff987-83da-4f83-9b49-39910b9fa1ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 45,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 24,
                "y": 49
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "1f9128e2-6a80-4d0b-9b48-0627473581fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 45,
                "offset": 2,
                "shift": 26,
                "w": 24,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "642328a0-f8cf-4a7f-abda-ec4707d3f6d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 45,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 45,
                "y": 49
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "4644c28c-a23f-4b3d-878e-19dfe0edf846",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 45,
                "offset": 4,
                "shift": 12,
                "w": 4,
                "x": 183,
                "y": 143
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "5f4e0b01-afd8-4015-9453-d14d34480b0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 45,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 65,
                "y": 143
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "5cb77e68-1c51-4989-8d43-e4ec3759775b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 45,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 77,
                "y": 143
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "49254c85-4e61-4d5e-84af-3196038f821d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 45,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 56,
                "y": 96
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "3ef4d984-5163-479f-8c1e-bd8096141d38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 45,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 333,
                "y": 96
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "5266041e-f725-44af-b0cf-65ab53914a1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 45,
                "offset": 3,
                "shift": 9,
                "w": 5,
                "x": 141,
                "y": 143
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "3c038c7b-203b-423a-9c96-7d05c0d86e96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 45,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 28,
                "y": 143
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "88d3da3c-64da-4288-8a04-a5239cdd003e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 45,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 148,
                "y": 143
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "c81f6b8e-1b1a-4f1b-8ba0-a5812ab690d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 45,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 350,
                "y": 96
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "3e3e6661-f8d1-4ffb-9fab-0fce081f7ea8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 45,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 108,
                "y": 49
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "286d52cc-555f-4791-996b-ed1687f088de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 45,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 15,
                "y": 143
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "42960021-d5cf-495f-a92e-f12bf0913c31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 74,
                "y": 96
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "0a1c0029-c8de-4a29-a62d-82c3db7ede6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 15,
                "x": 146,
                "y": 96
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "23b24da5-d115-4409-90a4-6d78d15ac8df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 45,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 66,
                "y": 49
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "d0b8d674-b597-44b9-b162-87fa7f5c8ada",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 211,
                "y": 49
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "7c69b0bf-7178-4b6a-a2c9-7bc515804ddf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 423,
                "y": 49
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "572afe8f-f882-4a69-be5f-a6b59c49dfbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 171,
                "y": 49
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "fbf1a825-af1b-4692-8fce-0876946b6642",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 442,
                "y": 49
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "92672b12-4de1-4866-856f-dc291d769fe5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 191,
                "y": 49
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "4806b9ea-4718-4ad5-b824-8518b819953f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 45,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 176,
                "y": 143
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "05e6ead3-33b2-48ac-91cf-68f8543c7224",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 45,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 133,
                "y": 143
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "631f8f79-8196-4317-af1a-39ee1cdd35ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 45,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 89,
                "y": 143
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "ca64d5eb-e77d-4de3-8f83-c2bef4921385",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 45,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 430,
                "y": 96
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "2f44d8e9-1157-424a-9fc9-9bb429a83166",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 45,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 460,
                "y": 96
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "e1b38cfe-d266-45f3-9377-e5e21ab42f32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 45,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 110,
                "y": 96
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c39a4fa8-7ef8-4c69-a5de-8fecfd7c1cbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 45,
                "offset": 1,
                "shift": 30,
                "w": 27,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "54f95f0d-a2c2-43e8-ba85-e5337a87f690",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 45,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 439,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b839b0ec-0916-4e3b-bdd2-ccd669cca261",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 385,
                "y": 49
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "243cc5e9-8e77-4dc3-9375-a2fde1d0e626",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 45,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 251,
                "y": 49
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "dea25511-01b8-4383-b94a-f49bff8c86cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 45,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 483,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "ed35e074-4d10-4023-80f1-f9829950114c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 45,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 404,
                "y": 49
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b468e78a-05c4-4be1-91f0-3b6e37c40b38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 45,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 347,
                "y": 49
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "728d9b78-b517-44a9-be4c-5d5aaafac8ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 45,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 371,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "83922921-bf50-42a1-9a36-7389ca6813ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 45,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 394,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "5a88cf3c-9a7d-4a39-847d-cf8b513a98fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 128,
                "y": 96
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "93161f92-aab4-49aa-b616-9b74f691d188",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 45,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 2,
                "y": 49
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "d12d40a0-6900-4a6e-b3ba-208a15d1e24f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 45,
                "offset": 3,
                "shift": 20,
                "w": 17,
                "x": 309,
                "y": 49
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "034677ef-891d-49d7-96a4-c37b23078c5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 45,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 290,
                "y": 49
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "fc2f2efa-6eed-4397-91c1-585de05e341e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 45,
                "offset": 1,
                "shift": 28,
                "w": 27,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "ccf27562-96cb-45fb-b66d-f1b79a60ab3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 45,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "fe9fe2df-8d71-4d7d-bf95-25747dc1bfea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 45,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "ab9fa0a5-e55c-4a8a-9757-cae8efec720e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 282,
                "y": 96
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "9a270907-6e1e-457d-a0de-ce31ce56d550",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 45,
                "offset": 1,
                "shift": 28,
                "w": 27,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "eb01fb31-590c-4d4e-a969-75eb38c093c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 45,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 150,
                "y": 49
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "62f85dd1-af8d-4e14-94cf-8e2dabb19301",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 45,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 87,
                "y": 49
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "44609750-4d2e-4127-9c26-6c927c0281b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 45,
                "offset": 1,
                "shift": 22,
                "w": 22,
                "x": 278,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "cf620907-1447-489a-a010-74de4818be71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 45,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 461,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "670df1ef-3a6e-4427-b177-611903595c5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 45,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 129,
                "y": 49
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "1a7dd511-c0d6-4554-9915-dfe1c3183832",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 45,
                "offset": 2,
                "shift": 33,
                "w": 31,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "761f0a1a-8184-4e8f-9957-b0d4aba27d96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 45,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 325,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "e6c6a8e8-4b28-44ca-9cb9-6960101bb773",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 45,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 417,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4a3eb656-cb8e-4775-8f84-6ec4502556ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 45,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 302,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "85c7ff08-6278-4ae3-b624-3769d8c9ce2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 45,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 101,
                "y": 143
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "29d53e3d-c893-4f10-ba59-c8a2a8970b55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 383,
                "y": 96
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "95b679fa-42d2-441f-a96f-eedc82da1600",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 45,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 112,
                "y": 143
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "2426561c-4226-4813-9774-7e44064559f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 45,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 415,
                "y": 96
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "83ea1699-be8d-41a9-b3bb-f522f5f55180",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 45,
                "offset": -1,
                "shift": 20,
                "w": 22,
                "x": 254,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "26ef4a6d-0d76-41f9-a15f-64c3b08c5882",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 8,
                "x": 123,
                "y": 143
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "52c9e67f-1aff-4563-8800-c2776264d99c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 45,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 38,
                "y": 96
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "8b0cce71-c40d-4058-a27d-9941d41c25db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 45,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 92,
                "y": 96
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "0412d6a0-af26-4dbf-b1c7-da13d7c2325f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 45,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 299,
                "y": 96
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "5b2573b4-edfd-4f36-a9fe-9375a83b1811",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 45,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 271,
                "y": 49
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "7dbeed8e-5116-4159-bf92-a441768699f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 45,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 20,
                "y": 96
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "a976fcff-a13f-44fa-a341-f126661e2028",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 45,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 367,
                "y": 96
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "decfb50f-0f7d-4ac7-b024-bcef3f1b7698",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 45,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 2,
                "y": 96
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "405b7b17-1147-4e83-9a5e-491a62f8fde4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 45,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 316,
                "y": 96
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "eec17990-0ce8-4a4b-9130-09409ffc454d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 45,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 162,
                "y": 143
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "10aff43a-31a3-4e37-a60e-a843c34983e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 45,
                "offset": -1,
                "shift": 13,
                "w": 12,
                "x": 474,
                "y": 96
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ec6a4dfd-761f-4549-af2d-2c4f180b49a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 45,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 265,
                "y": 96
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "7d513eec-32ef-4004-9534-4de1c6b9a08c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 45,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 155,
                "y": 143
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "14d2f493-945c-4164-91d5-89d3b954ea1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 45,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "90fc8ee7-859e-478a-94c7-87bc4befbc9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 248,
                "y": 96
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "47e58348-cea3-4c81-8098-34855a9f2eec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 231,
                "y": 96
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "de834cfa-cc30-4559-821d-a2134f272431",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 214,
                "y": 96
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "d6cf24d7-2f82-4681-a849-e4911d512560",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 45,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 197,
                "y": 96
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "7cbab29f-73e7-4f64-bfb8-459d78388916",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 45,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 445,
                "y": 96
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "5952b62f-67aa-4471-b44d-20ab45132ecd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 45,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 180,
                "y": 96
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e7457cac-a4cf-4082-bb90-96ade75577da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 45,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 399,
                "y": 96
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "7ca15b48-4871-4888-b36d-3b097b67d7be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 163,
                "y": 96
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "1cbc0945-3dcb-4dfd-82a1-a7ff07771295",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 45,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 479,
                "y": 49
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "9c7ea208-a207-4972-810b-618d61d6ac6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 45,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 348,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "8ca3683f-4a51-4cc0-b8e9-213ce6274afc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 45,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 231,
                "y": 49
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a7222131-26db-46d0-8b0f-2d6a10facae8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 45,
                "offset": -1,
                "shift": 17,
                "w": 17,
                "x": 328,
                "y": 49
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "7fb671f0-2ed7-4116-a841-e0cf52bd320c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 45,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 461,
                "y": 49
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "8465c050-505a-43a1-b865-9ea2e81cf848",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 45,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 488,
                "y": 96
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "593c3622-974e-4f07-a1c6-25d0b8976caa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 45,
                "offset": 5,
                "shift": 13,
                "w": 4,
                "x": 189,
                "y": 143
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "cb78a6b7-e1bd-4d71-9b45-155774bcefb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 45,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 143
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "05d86007-c8ba-47ef-a560-6b5a9164af10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 45,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 366,
                "y": 49
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
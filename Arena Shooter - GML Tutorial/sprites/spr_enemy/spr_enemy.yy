{
    "id": "d2b5dd98-ef62-4899-9b98-59feaaab4413",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 24,
    "bbox_right": 88,
    "bbox_top": 24,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c310d60b-3abc-4e75-9b22-6a711fac1d8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2b5dd98-ef62-4899-9b98-59feaaab4413",
            "compositeImage": {
                "id": "6bf6a8a2-d1e0-4bf4-9f86-5ce33568d1d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c310d60b-3abc-4e75-9b22-6a711fac1d8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f50b04d1-622b-43d6-aae1-193d16d2daa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c310d60b-3abc-4e75-9b22-6a711fac1d8b",
                    "LayerId": "a1ed006f-7cf7-49a5-a37b-fdd89c774c2a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 114,
    "layers": [
        {
            "id": "a1ed006f-7cf7-49a5-a37b-fdd89c774c2a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d2b5dd98-ef62-4899-9b98-59feaaab4413",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 57,
    "yorig": 57
}
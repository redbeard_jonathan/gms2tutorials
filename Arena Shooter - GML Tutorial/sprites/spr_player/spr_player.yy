{
    "id": "8b21c292-9f43-4c38-9fb5-7dba6ac270d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 85,
    "bbox_left": 31,
    "bbox_right": 82,
    "bbox_top": 31,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d3d2d1e-13e0-41ed-b4d2-acfd5202c87d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b21c292-9f43-4c38-9fb5-7dba6ac270d4",
            "compositeImage": {
                "id": "60ad72e9-7b22-4e0f-bc57-20ff1e56781e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d3d2d1e-13e0-41ed-b4d2-acfd5202c87d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67471181-4fd8-4c1e-9848-efb7653b5ed5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d3d2d1e-13e0-41ed-b4d2-acfd5202c87d",
                    "LayerId": "cecc4764-b9a5-45ac-8294-4566e3e018d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 115,
    "layers": [
        {
            "id": "cecc4764-b9a5-45ac-8294-4566e3e018d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b21c292-9f43-4c38-9fb5-7dba6ac270d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 114,
    "xorig": 57,
    "yorig": 57
}
{
    "id": "d86961dd-cd2b-44fd-bfc2-ff8eb315c688",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_darkTile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f1de47a3-142e-4363-8665-69541899771d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d86961dd-cd2b-44fd-bfc2-ff8eb315c688",
            "compositeImage": {
                "id": "32104cc5-c05d-4a3d-b351-0549601de4b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1de47a3-142e-4363-8665-69541899771d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1083cc81-9ada-4ff8-a172-fa092a92292a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1de47a3-142e-4363-8665-69541899771d",
                    "LayerId": "e573be86-d0ac-4711-8bb8-778a9f7665e8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "e573be86-d0ac-4711-8bb8-778a9f7665e8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d86961dd-cd2b-44fd-bfc2-ff8eb315c688",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "41f80468-8fd3-4797-a623-7f54745b4961",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 73,
    "bbox_left": 93,
    "bbox_right": 107,
    "bbox_top": 51,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85f2f731-06ae-42cd-8f76-53ae9fca435e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41f80468-8fd3-4797-a623-7f54745b4961",
            "compositeImage": {
                "id": "2f3ffb3f-a2c3-4191-859f-bbcb269d92ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85f2f731-06ae-42cd-8f76-53ae9fca435e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8074af11-c2bd-4421-87da-a5d4e5184781",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85f2f731-06ae-42cd-8f76-53ae9fca435e",
                    "LayerId": "1ecae02a-c365-4038-888d-4b360d0aa643"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "1ecae02a-c365-4038-888d-4b360d0aa643",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "41f80468-8fd3-4797-a623-7f54745b4961",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 102,
    "yorig": 62
}
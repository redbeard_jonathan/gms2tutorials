{
    "id": "b2d8e841-818d-4584-afea-ac0316e634d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 383,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "09a599a0-432c-432f-a280-3815dbaaf964",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2d8e841-818d-4584-afea-ac0316e634d0",
            "compositeImage": {
                "id": "f2067850-80b5-4329-96fb-6d0ed69b02c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09a599a0-432c-432f-a280-3815dbaaf964",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b60cbc84-b6a9-4eb6-892a-18618b79a97e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09a599a0-432c-432f-a280-3815dbaaf964",
                    "LayerId": "9ba1342c-9c33-4346-b400-974d0e461883"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "9ba1342c-9c33-4346-b400-974d0e461883",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b2d8e841-818d-4584-afea-ac0316e634d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 198,
    "yorig": 295
}
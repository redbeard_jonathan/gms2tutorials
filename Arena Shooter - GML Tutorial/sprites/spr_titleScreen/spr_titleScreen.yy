{
    "id": "140e440d-d491-4b4b-88e1-fcdf7c3871a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_titleScreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 282,
    "bbox_left": 0,
    "bbox_right": 859,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a932cbf1-66ec-4716-a485-0e369c882240",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "140e440d-d491-4b4b-88e1-fcdf7c3871a5",
            "compositeImage": {
                "id": "22748971-949c-4ffc-aa1c-67a7c4fcf1b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a932cbf1-66ec-4716-a485-0e369c882240",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2197782-5515-480a-a226-406dc0f7c670",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a932cbf1-66ec-4716-a485-0e369c882240",
                    "LayerId": "6a167c5c-aeb6-460e-8c60-fe8b477ed8f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 283,
    "layers": [
        {
            "id": "6a167c5c-aeb6-460e-8c60-fe8b477ed8f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "140e440d-d491-4b4b-88e1-fcdf7c3871a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 860,
    "xorig": 430,
    "yorig": 141
}
{
    "id": "2d09de48-aa92-4db4-81ed-923a7e243973",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_title",
    "eventList": [
        {
            "id": "29774a98-0a65-4848-a3c5-622118a43b7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2d09de48-aa92-4db4-81ed-923a7e243973"
        },
        {
            "id": "c5cc06f7-9c3a-4fd4-b68a-c61e0df10e11",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2d09de48-aa92-4db4-81ed-923a7e243973"
        },
        {
            "id": "58a94840-6755-4865-9b3b-2e114ba8053d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 10,
            "m_owner": "2d09de48-aa92-4db4-81ed-923a7e243973"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "140e440d-d491-4b4b-88e1-fcdf7c3871a5",
    "visible": true
}
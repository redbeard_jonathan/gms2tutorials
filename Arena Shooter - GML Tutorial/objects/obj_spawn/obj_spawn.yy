{
    "id": "dd0eea36-2ede-4779-a1e4-878f4febdec7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_spawn",
    "eventList": [
        {
            "id": "8818e363-617f-435d-8521-08187fd370d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dd0eea36-2ede-4779-a1e4-878f4febdec7"
        },
        {
            "id": "14e1dc93-42ca-4f24-b3c1-1e6459246281",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dd0eea36-2ede-4779-a1e4-878f4febdec7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d2b5dd98-ef62-4899-9b98-59feaaab4413",
    "visible": true
}
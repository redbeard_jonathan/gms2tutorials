{
    "id": "30da578d-a175-4148-ad4e-5fb5d2b57fa8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "b7851a0c-8155-42e3-9aec-f9904601f6da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "30da578d-a175-4148-ad4e-5fb5d2b57fa8"
        },
        {
            "id": "83f6f785-2fd6-4759-bdbf-8ef7c3c4578e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "bca83702-45c0-4702-8e27-435b2e55ec8d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "30da578d-a175-4148-ad4e-5fb5d2b57fa8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "41f80468-8fd3-4797-a623-7f54745b4961",
    "visible": true
}
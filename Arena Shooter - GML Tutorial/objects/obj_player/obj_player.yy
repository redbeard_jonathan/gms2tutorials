{
    "id": "f8f2d64e-0aa3-4282-9bee-57b2674d7603",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "33815025-e71a-4262-bd49-e434e85f7f11",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f8f2d64e-0aa3-4282-9bee-57b2674d7603"
        },
        {
            "id": "d0857ccd-f936-4e90-89e9-4222bba95f38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f8f2d64e-0aa3-4282-9bee-57b2674d7603"
        },
        {
            "id": "4d7221e1-4cb4-45ad-bbf3-2ee8b0b09888",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "bca83702-45c0-4702-8e27-435b2e55ec8d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f8f2d64e-0aa3-4282-9bee-57b2674d7603"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8b21c292-9f43-4c38-9fb5-7dba6ac270d4",
    "visible": true
}
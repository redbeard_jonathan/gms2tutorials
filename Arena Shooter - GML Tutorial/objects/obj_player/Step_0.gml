/// @description Insert description here
//Player movement

var horizontalSpriteOffset = sprite_width / 2;
var verticalSpriteOffset = sprite_height / 2;

if(keyboard_check(vk_left))
{
    var newX = x - 4;
    if(newX - horizontalSpriteOffset <= 0)
    {
        x = horizontalSpriteOffset;    
    }
	else
	{
	    x = newX;
	}
}

if(keyboard_check(vk_right))
{
    var newX = x + 4;
    if(newX + horizontalSpriteOffset >= room_width)
    {
        x = room_width - horizontalSpriteOffset;    
    }
	else
	{
	    x = newX;
	}
}

if(keyboard_check(vk_up))
{
    var newY = y - 4;
    if(newY - verticalSpriteOffset <= 0)
    {
        y = verticalSpriteOffset;    
    }
	else
	{
	    y = newY;
	}
}

if(keyboard_check(vk_down))
{
    var newY = y + 4;
    if(newY + verticalSpriteOffset >= room_height)
    {
        y = room_height - verticalSpriteOffset;
    }
	else
	{
	    y = newY;
	}
}

image_angle = point_direction(x, y, mouse_x, mouse_y);

//Shooting

if(mouse_check_button(mb_left)) && (cooldown < 1)
{
	instance_create_layer(x, y, "Bullets", obj_bullet);
	cooldown = 3;
}

cooldown -= 1;

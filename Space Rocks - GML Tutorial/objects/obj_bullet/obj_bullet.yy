{
    "id": "db126530-ca8a-4d4f-91ab-c3b3e6fc6786",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "7f25ca57-d8f6-47e4-a4f9-b23638dec2dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "db126530-ca8a-4d4f-91ab-c3b3e6fc6786"
        },
        {
            "id": "769e55ed-82cb-4eb9-a468-d7a38b4b27e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d15b7755-4c8a-4955-b513-0e18a0894a97",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "db126530-ca8a-4d4f-91ab-c3b3e6fc6786"
        },
        {
            "id": "c9a5e0a0-adff-4f92-83ba-4c05c6fe174f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "db126530-ca8a-4d4f-91ab-c3b3e6fc6786"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "548f3de9-279e-465d-a717-e4f867a9b1de",
    "visible": true
}
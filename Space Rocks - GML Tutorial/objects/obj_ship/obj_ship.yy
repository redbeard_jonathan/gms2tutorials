{
    "id": "a3aacb22-9f8c-4455-b537-2293ceab910c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship",
    "eventList": [
        {
            "id": "6f6cc64a-8066-4f69-bce2-37dcb98423f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a3aacb22-9f8c-4455-b537-2293ceab910c"
        },
        {
            "id": "cd19afce-7855-468a-b235-32cfded3bfb3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d15b7755-4c8a-4955-b513-0e18a0894a97",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a3aacb22-9f8c-4455-b537-2293ceab910c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cf089eb0-e8c9-4238-afea-49218842a509",
    "visible": true
}
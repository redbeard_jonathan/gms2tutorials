{
    "id": "e940a427-2683-4c3e-8cf6-b0d1cfdb91c9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_debris",
    "eventList": [
        {
            "id": "5db4c187-dffe-4bed-8ebd-6558a89f60e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e940a427-2683-4c3e-8cf6-b0d1cfdb91c9"
        },
        {
            "id": "862ed437-b050-42e9-a7f0-551335000167",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e940a427-2683-4c3e-8cf6-b0d1cfdb91c9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b433ba06-d619-438b-8b88-0baf6a05c7ef",
    "visible": true
}
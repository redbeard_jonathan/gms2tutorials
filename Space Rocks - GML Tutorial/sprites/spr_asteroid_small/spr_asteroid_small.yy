{
    "id": "bbc5f677-8150-4ff9-9597-f7479c12b829",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85d190ac-f596-4765-9ff8-efafe75458d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbc5f677-8150-4ff9-9597-f7479c12b829",
            "compositeImage": {
                "id": "d28fab71-ca92-4be9-8f46-6f39ea7c56f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85d190ac-f596-4765-9ff8-efafe75458d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fac9a49e-27be-40a6-a0d0-0cb4685c0fe5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85d190ac-f596-4765-9ff8-efafe75458d8",
                    "LayerId": "65907af6-077f-442c-9dca-4ace8e4b953e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "65907af6-077f-442c-9dca-4ace8e4b953e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bbc5f677-8150-4ff9-9597-f7479c12b829",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
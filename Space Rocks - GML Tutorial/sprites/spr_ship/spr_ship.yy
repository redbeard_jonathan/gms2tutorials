{
    "id": "cf089eb0-e8c9-4238-afea-49218842a509",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 0,
    "bbox_right": 32,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a1e25e5f-cde0-41f4-9686-99b847159855",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf089eb0-e8c9-4238-afea-49218842a509",
            "compositeImage": {
                "id": "c774d5aa-d162-4795-82e8-cb57167f7627",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1e25e5f-cde0-41f4-9686-99b847159855",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7e3e58f-73b7-4c9b-a506-1ec05e380a41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1e25e5f-cde0-41f4-9686-99b847159855",
                    "LayerId": "c99afc1b-05cd-4d67-8bba-5a3b00623658"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 32,
    "layers": [
        {
            "id": "c99afc1b-05cd-4d67-8bba-5a3b00623658",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cf089eb0-e8c9-4238-afea-49218842a509",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
{
    "id": "b433ba06-d619-438b-8b88-0baf6a05c7ef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_debris",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f12d93a3-276b-4850-a230-4934fd34d7d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b433ba06-d619-438b-8b88-0baf6a05c7ef",
            "compositeImage": {
                "id": "74b27202-d235-4bc9-bca7-c7e763a91656",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f12d93a3-276b-4850-a230-4934fd34d7d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b43a07b3-f76c-4445-9ca8-91a4de414c8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f12d93a3-276b-4850-a230-4934fd34d7d6",
                    "LayerId": "94f3a06d-ab15-479b-8bac-6429f7f3c1c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "94f3a06d-ab15-479b-8bac-6429f7f3c1c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b433ba06-d619-438b-8b88-0baf6a05c7ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "548f3de9-279e-465d-a717-e4f867a9b1de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7866b12d-1c0a-4ad4-aa7a-5cc54a891050",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "548f3de9-279e-465d-a717-e4f867a9b1de",
            "compositeImage": {
                "id": "20765198-fbd4-4ec3-9810-63e4a6aacd86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7866b12d-1c0a-4ad4-aa7a-5cc54a891050",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc48399a-f35e-4b46-b581-bb4d47a13139",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7866b12d-1c0a-4ad4-aa7a-5cc54a891050",
                    "LayerId": "9d775549-c0e3-4577-91a3-7dcc3728f5df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "9d775549-c0e3-4577-91a3-7dcc3728f5df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "548f3de9-279e-465d-a717-e4f867a9b1de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 1,
    "yorig": 1
}
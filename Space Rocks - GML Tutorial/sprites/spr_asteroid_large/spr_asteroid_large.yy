{
    "id": "25450190-cdc4-4086-9e36-90bb6c357df4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_large",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5ce09636-9bac-4a5e-9a95-2691aa18b452",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25450190-cdc4-4086-9e36-90bb6c357df4",
            "compositeImage": {
                "id": "bd859249-8a35-4629-a9df-745482a01b4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ce09636-9bac-4a5e-9a95-2691aa18b452",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3e1d474-6e88-49e3-87f7-7858690385c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ce09636-9bac-4a5e-9a95-2691aa18b452",
                    "LayerId": "1645359e-c294-4980-9aa8-f54d4c4818e0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1645359e-c294-4980-9aa8-f54d4c4818e0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "25450190-cdc4-4086-9e36-90bb6c357df4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
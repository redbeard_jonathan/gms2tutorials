{
    "id": "922196b5-5c0c-4c2f-b003-696e954fe8da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_medium",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0b531db6-4514-46e7-bdf6-0ae15014349b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "922196b5-5c0c-4c2f-b003-696e954fe8da",
            "compositeImage": {
                "id": "2bdc52d9-29b4-4187-bc82-14ea0e1b4a3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b531db6-4514-46e7-bdf6-0ae15014349b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "776b87e2-d058-4984-8e91-f69a25e525c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b531db6-4514-46e7-bdf6-0ae15014349b",
                    "LayerId": "983a1026-e64f-4b4c-8919-808616687cb2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "983a1026-e64f-4b4c-8919-808616687cb2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "922196b5-5c0c-4c2f-b003-696e954fe8da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
{
    "id": "b675467c-5086-4bbc-9bab-8460843bbffa",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_text",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Consolas",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "27920ee4-f432-4ce2-8f0f-47b541237ed3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "a08bc225-737e-49e6-b4c6-70560d49826d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 136,
                "y": 65
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "c988d785-a780-4ccc-b23b-66e701885730",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 98,
                "y": 65
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "4f01a38f-55bc-4cfc-9201-f004f35526d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 23
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "8b0c79af-9a5b-4e59-8c22-11438a0d7f69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 227,
                "y": 23
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "13db4a22-8ee5-4bdc-8804-6cd4aebd9c0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 23
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "2e3d96dd-1234-4b48-8270-10774990c9cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 23
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "9ebc630c-9a46-45cd-ac30-bc9b154f7836",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 4,
                "shift": 9,
                "w": 3,
                "x": 131,
                "y": 65
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b53dee72-5553-45d0-bb98-75d9a8d16af0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 112,
                "y": 65
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "057574c6-8eb6-4883-825d-179e56158f92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 119,
                "y": 65
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "20e14dda-5dff-424e-bd7b-82d56bc89b7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 140,
                "y": 44
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "82ac44d0-cb77-4179-bbf9-778e06e75d33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "e34acc0f-9542-46d5-8f68-8fa035acfc1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 77,
                "y": 65
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "4748cefd-e3cb-4cdb-aa98-0a1a308459d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 70,
                "y": 65
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "543ac38f-f043-498b-9598-fc6346b05ada",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 126,
                "y": 65
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "e8e149be-3436-478d-8ffd-d0967d75c229",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 147,
                "y": 23
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "7d7ecb49-a6de-4432-8985-f3a53b7b28be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 23
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "35754ce9-ac38-4f10-8077-f1d279518fe3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 212,
                "y": 44
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "39419443-f94e-4e6a-b939-ccc1496e8933",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 194,
                "y": 44
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "ab3c8cf9-f574-45f6-ae1b-14a0e594d993",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 185,
                "y": 44
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "31149f39-3d75-42cc-a7f1-9359868236cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 244,
                "y": 2
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "418571d3-2466-49c6-ba08-8684bea4d0fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "464ff027-20c3-4def-9c79-e38a5fb090b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 77,
                "y": 23
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "8eca1b95-d0fe-467c-beb2-0b13937e7abd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 87,
                "y": 23
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "9b4782ee-53f1-4ac0-8c7b-38427ecc223d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 167,
                "y": 23
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "88cd1f8f-34c3-424b-b3c4-2f0b70592ea1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "89defac9-207f-43c8-be44-24c610214e96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 3,
                "x": 141,
                "y": 65
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "4a0b400a-2be3-448a-85f7-2551cf236374",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 105,
                "y": 65
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a90b2ee7-423a-4f49-baf5-d3c8fbc42549",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 158,
                "y": 44
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "94337753-8fe6-4e3f-8a43-c1a5daad71f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 167,
                "y": 44
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "95b51aa9-534b-4b0c-99ad-211efb773a57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 176,
                "y": 44
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "655b2db4-ba06-4540-a619-ebedbc14440e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 54,
                "y": 65
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "6f877ccc-d840-4208-925d-b37bac9517f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "950e39b3-180b-4b78-a170-bc797ad590db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "52f07add-7cf1-4a64-beb4-cfae48a92113",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 203,
                "y": 44
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "50b8e9f8-056a-4490-a777-5468465baf55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 197,
                "y": 23
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "265ac195-ed1e-4bae-901c-1605a59c367b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c05339e0-fb6b-4582-a097-ec2aeb6a2829",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 221,
                "y": 44
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "2320e55b-494e-44e9-909d-662be9c30042",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 149,
                "y": 44
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "44f4e9ea-46d4-4dbe-ac5d-6a1e1160d5f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 217,
                "y": 23
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "300a8de0-ef97-4a67-86c7-cedea2381898",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 207,
                "y": 23
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "737a19e7-665a-4ef4-a99a-224796c3640d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 230,
                "y": 44
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "2f431d74-e52f-4f8f-9c99-dede43a13139",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 62,
                "y": 65
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "17a75c6c-be53-49a9-9a98-4be6c51358d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 177,
                "y": 23
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "5d229fcb-1691-449d-b40d-a0cecf8d6b9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 239,
                "y": 44
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "6e8ada48-3846-46d1-8052-857b3fe01d20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "4df29ab1-db74-48ae-a96c-b21a1025edf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 157,
                "y": 23
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "579391e7-8472-4302-b114-12e02b35122a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "9f420bd0-7feb-4d98-be4c-299f0618c3a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 20,
                "y": 65
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d4d190b9-2eb0-457e-9eb2-86ead17af894",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "11a3b1a0-bf4c-4ca1-aa56-a56c1a05b946",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 137,
                "y": 23
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "ea8e533b-b60a-40f2-8b44-f1c96ed33a90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 127,
                "y": 23
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "f9cd3ad3-4baf-48eb-98be-cc4022aeae66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "10ec1538-fdc9-49c6-8c4c-b16423b8e0a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 107,
                "y": 23
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "ed67a054-148d-425a-8fad-82c17d0497fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a9f7506a-3fc6-4606-85cd-2af4b546cea0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "b0702cec-8f30-4252-89cc-f22157804058",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "5e5a7f16-ae6d-4154-9007-7d66e66462e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "fd6895cd-eda1-45e9-b5c4-4dbc66dd079b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 67,
                "y": 23
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "5b652d14-9704-41c1-8dab-af8ffcaed65d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 3,
                "shift": 9,
                "w": 5,
                "x": 84,
                "y": 65
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "c9e04764-023e-432f-b25c-b7486e9eccab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 29,
                "y": 65
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "93bc2837-79e2-48f0-852a-4ecc5249327b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 91,
                "y": 65
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "dbe3687b-2757-4169-b45b-b6db5ba5ecf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 187,
                "y": 23
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "b30e09b0-dc68-4656-9123-e766a6fa1d1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "a345d451-21d0-42dd-9be8-2c44bf4a0fff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 6,
                "x": 38,
                "y": 65
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "3d330630-102c-4d2b-b5b5-f732613b8139",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 122,
                "y": 44
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "6067d59d-cc89-4bcd-b908-7b1120a09458",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 32,
                "y": 44
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "c2a860d7-5342-4100-8edc-6f2ef4a16475",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 50,
                "y": 44
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "43562891-d280-41e8-8cc1-f76da9bc0cdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 97,
                "y": 23
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "75edd91d-59dc-4bee-b318-ebad81d1bc1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 117,
                "y": 23
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "5280c58e-936f-417b-aaed-4f171b4eed7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "5c104ab6-3af3-4290-82cd-f86cdb464ed5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "0b7cc271-d018-4e62-980e-9cd8bd1e8661",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 86,
                "y": 44
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "43c14dd1-301b-4149-b8ef-38343b0540e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 95,
                "y": 44
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "e07fa480-a968-4b52-9327-a5fd67508463",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 46,
                "y": 65
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "dc36da99-956c-46d6-98ec-b17e39385316",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 57,
                "y": 23
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "ec98b233-9231-427b-bb98-e8604e531c78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 68,
                "y": 44
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "9caa8f2c-f5be-43f2-9bab-e8959829d9b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a39075e0-e1ff-4d0d-a7ca-78de04d8d294",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 104,
                "y": 44
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "55a6eb36-2905-4f18-a513-aafb702cfe30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 145,
                "y": 2
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "8a38ad21-e987-4be6-9420-5914ecf68a2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 77,
                "y": 44
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "8a950aff-ad40-4fca-be98-193559aff187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 12,
                "y": 44
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "e58960c9-2dc6-435f-8ebb-7dd1e8ae28da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 8,
                "x": 22,
                "y": 44
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "4e2bc2dc-8a4b-42fa-9ffb-17164c0eaab2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 59,
                "y": 44
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "499da1ae-e652-434e-933c-9117bdb07ac1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 237,
                "y": 23
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "01af09ea-178a-4365-b07c-0413b58c71c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 41,
                "y": 44
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "3c30601a-7c19-4da2-9a88-0f105dac5e8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 156,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "710735a5-0c55-4a3f-addb-cd9ae0178c40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "739c69bc-b75b-4c64-abc0-f2d162b9faa6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "acce461e-db75-4d95-a4fc-42e87d27f1f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "df4760a0-0ef7-495b-bf3c-50826f8b4c1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 113,
                "y": 44
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "c3cbaa71-3f4d-4045-8929-3a7e55a0d976",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 131,
                "y": 44
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "90b234b3-ea47-4507-8df0-1f10e29b02fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 4,
                "shift": 9,
                "w": 3,
                "x": 146,
                "y": 65
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "2b68b619-844f-49a8-a1ad-2c8eb343f689",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 11,
                "y": 65
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b20d4930-6d62-4ac1-8377-115b74666673",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 233,
                "y": 2
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
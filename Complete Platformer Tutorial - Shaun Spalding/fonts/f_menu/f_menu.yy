{
    "id": "a71c2487-eded-4347-911f-5d0c3b877459",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "f_menu",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "m5x7",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "94154182-291d-48c8-9c1a-2347432b5a4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "29f33314-a7ed-42fe-9e7a-a516b9a729b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 28,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 238,
                "y": 62
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "2d665c2d-128c-4e67-958e-8991a1161ba0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 230,
                "y": 62
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "f7ffcec2-261f-449b-8000-cee5b64fb21b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 218,
                "y": 62
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "4fe2f06f-23b8-452b-8fe9-0003a590b268",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 206,
                "y": 62
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "2aaeda4f-a299-4534-9fff-abc9f4a52fb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 194,
                "y": 62
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "ee8697db-18b8-4233-89bc-ad601300d57d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 28,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 180,
                "y": 62
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "618c906a-77ef-4ed2-875c-f9f7ba90386b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 28,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 176,
                "y": 62
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "c8c14966-dfb1-481b-99f3-36c47fd8bfad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 170,
                "y": 62
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "aabe8fa9-e842-46c7-b6ba-78c98f1f4154",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 164,
                "y": 62
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "5c493256-8376-4169-9090-08768407cbd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 242,
                "y": 62
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "79282f2d-cd96-475d-ab7b-53a6629b21ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 152,
                "y": 62
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "63f8d120-e98f-4b06-aa05-8c6a99e84672",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 134,
                "y": 62
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "58670588-7540-46ad-87b6-66bfa80b3252",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 122,
                "y": 62
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "eb90b793-fc11-484d-af13-23ecf8339714",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 28,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 118,
                "y": 62
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "68823b0a-7590-446e-b09f-ad6a3c35afba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 110,
                "y": 62
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "d4010dfe-0fe5-4a72-aeab-40f090a17f59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 98,
                "y": 62
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "de9ff5e9-47a0-47b5-a4f5-67c7209a8b55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 86,
                "y": 62
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "542eda47-8f61-432a-8ee1-09cb6d8a2ee0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 74,
                "y": 62
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "49fea3f7-61cf-454b-a2d8-42690d800960",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 62,
                "y": 62
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9e43fa43-793d-4e1f-b711-30174ecc9de1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 50,
                "y": 62
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "cd60a3ce-cd83-4f42-9314-b4cc8c9ffd41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 140,
                "y": 62
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "237cfbd3-5cd3-45bb-8a89-365cb46f8e9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "81956792-740d-4ae4-9525-2d132ca7063a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 92
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "f2496b90-5a73-4198-8023-b815fe8f5f6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 26,
                "y": 92
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "1a5eadc1-7042-436c-a649-0dee77e8162a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 6,
                "y": 122
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "4aef022b-dbf9-4c5c-b6a4-2fed3dc5d098",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 28,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "a097dbe0-1f2d-4c7e-9435-47673c68d9c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 248,
                "y": 92
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a5915b26-dacf-435a-b523-925abeba597f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 240,
                "y": 92
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "34f932dc-56d2-4af3-9582-0a602abf4521",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 230,
                "y": 92
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d4f077cb-d4cf-4b98-bfec-1c164375b34f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 222,
                "y": 92
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "0c1d20aa-1e69-40d8-b18a-248b0c8f0071",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 210,
                "y": 92
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "7b026b70-21fd-4fe3-a0f5-9e6b95978b58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 198,
                "y": 92
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "e35728db-66b1-4b0b-988d-387bca383560",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 186,
                "y": 92
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "69eec121-e65f-446c-9d96-e6323b6fd703",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 174,
                "y": 92
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "53d672ad-ee0c-40aa-8f7a-86ed017c191d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 162,
                "y": 92
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "7bd67bea-f043-4c9b-b016-bc102d2b98cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 150,
                "y": 92
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "6d6948b2-678d-459d-b7b7-ca114b1f32c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 138,
                "y": 92
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "2e43bcaa-7e51-4216-b411-76808fc01b54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 126,
                "y": 92
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "9b3040ab-d340-4f7d-9f58-37fa37011e89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 114,
                "y": 92
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "e5f8434b-c05a-4007-8f34-b1ccbda6e929",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 102,
                "y": 92
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "ff6720f5-22d8-4046-a5eb-f00a7255ceb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 90,
                "y": 92
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "27ce0a18-b29e-4f17-9108-6f6ff81a42e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 78,
                "y": 92
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "465413da-666f-4638-b1e5-276c247b727e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 66,
                "y": 92
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "1a8c3617-a519-4ae6-b483-280af6811305",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 54,
                "y": 92
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "11e4776b-e473-48b6-a8c0-b5a2de064198",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 28,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 38,
                "y": 92
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "9ef26a68-8c40-421f-94a8-cfb7d0e1915b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 38,
                "y": 62
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "66ee97d8-db44-43c0-8a5f-be843a587ce5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 26,
                "y": 62
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "51846fe4-1bc6-465d-8ca0-ee91d8241088",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 62
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "80d5dc90-4ef1-49f1-96f4-e159759c2a3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "19e94abe-e8fb-4fea-9945-ee008fd79826",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 226,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "a8422ebb-5407-4a70-bc7c-ba4b76d0be86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 214,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "ce84f84d-f05c-4788-a291-862bc9ef9423",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 202,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "cae2b8e1-cb45-41e8-a3a3-d05c64ca2f4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "307d1913-d32d-4618-bd13-e96690d55a1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "d756ff0e-e0eb-4221-9576-2b1919a35549",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 28,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "b54e3422-10f0-4234-810f-41a1f2670184",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "3781d783-32f6-4327-90b9-08f1afec1bdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 138,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "6b1b5138-7971-40af-966b-b37456052ee8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "78e71b9c-6e2e-43dc-8a55-814c5ec61405",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "b2a564d2-d5bf-4ae7-b44b-7d6ffb4926d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "878707cd-14f8-40df-977e-46a489d30af5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "d15cd8b1-40fe-4ceb-9b06-c24fc3ee3211",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "e956cfd6-2620-4b54-a70f-a3906d359535",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "bd183a13-92d9-42ed-93a0-ea165b663f9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "64e7878d-b1fc-4189-b533-823741bbfde4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "ac8c1e5f-bf88-4410-8113-8dd392b86f26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "4bcc9356-7146-4dc2-964a-c64a3be14d29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "b20de0d3-4856-4682-88eb-d08ca0d4a1f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "b67c3901-4758-402f-af27-a3cf46267ef3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "aa8ce5a9-8c64-4b1b-9967-5ab198313b35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "0b68622e-9ee0-41e7-8392-7b932e758bca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 32
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "79a03de8-f543-4b6e-a6fd-4477fe708053",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 128,
                "y": 32
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "632cb5a9-dee6-4e3f-b3ef-b34e317f901f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 26,
                "y": 32
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "29183e25-822e-4b86-bc10-0f13eec7d07d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 240,
                "y": 32
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "959cacc7-03c3-4120-83d1-c25477656070",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 230,
                "y": 32
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "177d6635-4cfe-4f79-b42e-eb1731c485bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 28,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 224,
                "y": 32
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "b4c26970-4584-49ed-be64-a4076cad9a69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 28,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 208,
                "y": 32
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "d38f1497-eedd-42e0-98d6-aa5859dd8a6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 196,
                "y": 32
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "12abff23-3aa7-4367-9449-ec4bbaafc9db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 184,
                "y": 32
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "e2452a3d-fb92-4a01-90a4-1b1b8479e459",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 172,
                "y": 32
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "21e768d1-7ff0-4d5e-a587-c739aff69e3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 160,
                "y": 32
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "0d4a791c-def3-4e03-97e5-aa1372b2b5e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 150,
                "y": 32
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "fe3e0b90-b387-4031-925f-61d665b5d5a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "2bf732b1-52f9-4a95-9d73-634fd8c3ab92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 140,
                "y": 32
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "44758107-9d4d-45bf-a12e-b148113b3fb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 116,
                "y": 32
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "4912a770-010d-49af-81bb-363f2f6d37c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 104,
                "y": 32
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "55b0f384-9683-4670-b7df-888cdaf9f641",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 28,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 88,
                "y": 32
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "2e0566c8-b55a-49f1-8d59-11be2667219f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 76,
                "y": 32
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "d3e30be7-d160-4e65-a246-525659877d33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 64,
                "y": 32
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "ea74b1f2-9e15-4b73-913c-b2b71f323e0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 52,
                "y": 32
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "8179b590-26bb-4479-9144-1efa9e6c0cff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 44,
                "y": 32
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "ba292569-deb1-44c8-b358-90b5e09424b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 28,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 40,
                "y": 32
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "bf2a60b3-f592-4e6e-a849-d3a63533317a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 32,
                "y": 32
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "2f896c1c-31c9-4349-b8fe-15fe16c9bed3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 18,
                "y": 122
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "f127ec44-5388-4465-9cdd-730e1e1ad3f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 28,
                "offset": 6,
                "shift": 31,
                "w": 19,
                "x": 30,
                "y": 122
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 24,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
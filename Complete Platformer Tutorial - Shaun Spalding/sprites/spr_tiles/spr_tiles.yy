{
    "id": "e12d4b62-fc0e-41d1-b1d7-69e0710bb863",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "df4fd1c9-245c-4be4-95e7-f8644573fb8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e12d4b62-fc0e-41d1-b1d7-69e0710bb863",
            "compositeImage": {
                "id": "25bbf576-44e5-4571-a9d2-d544b2fab16b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df4fd1c9-245c-4be4-95e7-f8644573fb8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b46c3176-e85b-493e-9aeb-441c11e09987",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df4fd1c9-245c-4be4-95e7-f8644573fb8c",
                    "LayerId": "6b3bd2e5-2e95-4ec0-9fb0-83dbe180dd86"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "6b3bd2e5-2e95-4ec0-9fb0-83dbe180dd86",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e12d4b62-fc0e-41d1-b1d7-69e0710bb863",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 96
}
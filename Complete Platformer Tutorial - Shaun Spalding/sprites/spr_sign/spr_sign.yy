{
    "id": "1f375c28-4f34-4d43-af22-f758bbf014fb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sign",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 8,
    "bbox_right": 43,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ecece82e-3b04-44f0-92f0-ac34a2a56583",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f375c28-4f34-4d43-af22-f758bbf014fb",
            "compositeImage": {
                "id": "f21ccec8-f350-4031-afba-4514fca5ee75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecece82e-3b04-44f0-92f0-ac34a2a56583",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6e66253-b1c0-445f-a090-8a4196982af0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecece82e-3b04-44f0-92f0-ac34a2a56583",
                    "LayerId": "ff283252-63a1-4a54-94a0-92d66a7bdd7b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "ff283252-63a1-4a54-94a0-92d66a7bdd7b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f375c28-4f34-4d43-af22-f758bbf014fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}
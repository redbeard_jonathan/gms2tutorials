{
    "id": "e4fa594c-42fc-447a-b16b-8fa025d0603c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 250,
    "bbox_left": 182,
    "bbox_right": 449,
    "bbox_top": 71,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d6da5199-7b46-4abf-90c7-b0c7fff529e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4fa594c-42fc-447a-b16b-8fa025d0603c",
            "compositeImage": {
                "id": "67fb26f3-16af-4f30-9af1-09e8ba82b49a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6da5199-7b46-4abf-90c7-b0c7fff529e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3916a24-f392-4eb5-88c0-27c0c08282c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6da5199-7b46-4abf-90c7-b0c7fff529e7",
                    "LayerId": "cb1e3414-4ed5-47aa-880e-60fb91bb8ff0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 302,
    "layers": [
        {
            "id": "cb1e3414-4ed5-47aa-880e-60fb91bb8ff0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4fa594c-42fc-447a-b16b-8fa025d0603c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 610,
    "xorig": 0,
    "yorig": 0
}
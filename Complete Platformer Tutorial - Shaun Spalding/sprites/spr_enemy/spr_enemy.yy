{
    "id": "5aec0987-c050-4979-9e7d-12cafe804a84",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 19,
    "bbox_right": 29,
    "bbox_top": 18,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dc1c8536-4280-4f9d-831c-311a7efbd9cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aec0987-c050-4979-9e7d-12cafe804a84",
            "compositeImage": {
                "id": "8b2d76a6-0bed-47a1-90bb-ac3405a71cb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc1c8536-4280-4f9d-831c-311a7efbd9cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a47d7e5a-0975-4359-bb12-f5f72c65912b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc1c8536-4280-4f9d-831c-311a7efbd9cb",
                    "LayerId": "1847a987-f8da-4ea7-8c44-5290ae099915"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "1847a987-f8da-4ea7-8c44-5290ae099915",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5aec0987-c050-4979-9e7d-12cafe804a84",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}
{
    "id": "cd62c7b5-ea5c-4f46-afb1-77523f431cdc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 19,
    "bbox_right": 32,
    "bbox_top": 35,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d7f3a54d-47c7-40ba-9d0b-bec77529882b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd62c7b5-ea5c-4f46-afb1-77523f431cdc",
            "compositeImage": {
                "id": "2c0b6a9a-aeb4-49c4-8c16-43b5961fcd30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7f3a54d-47c7-40ba-9d0b-bec77529882b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70cc46b5-9a53-4fa1-bbe4-6a52ca21fee2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7f3a54d-47c7-40ba-9d0b-bec77529882b",
                    "LayerId": "d3de0733-13e5-48f2-86fd-7ec5d1b81252"
                }
            ]
        },
        {
            "id": "05eaa71b-c3b2-4f6a-8f03-5adf123d4a47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd62c7b5-ea5c-4f46-afb1-77523f431cdc",
            "compositeImage": {
                "id": "16e7fe7d-e092-40d2-a59a-7b8d4f59db17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05eaa71b-c3b2-4f6a-8f03-5adf123d4a47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3183fd0e-a75a-4f12-b765-663e49e20ded",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05eaa71b-c3b2-4f6a-8f03-5adf123d4a47",
                    "LayerId": "d3de0733-13e5-48f2-86fd-7ec5d1b81252"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "d3de0733-13e5-48f2-86fd-7ec5d1b81252",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd62c7b5-ea5c-4f46-afb1-77523f431cdc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 21,
    "yorig": 40
}
{
    "id": "c153a42a-6615-45aa-9ea2-83466ad49604",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_running_backwards",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 7,
    "bbox_right": 40,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b7a88e04-21fe-456d-bb46-5f9f06fc72ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c153a42a-6615-45aa-9ea2-83466ad49604",
            "compositeImage": {
                "id": "2c29d5fb-8e3d-4bc7-a951-b502b9d71022",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7a88e04-21fe-456d-bb46-5f9f06fc72ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "536ba450-0066-4aae-9e6d-c691a37feb6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7a88e04-21fe-456d-bb46-5f9f06fc72ce",
                    "LayerId": "d401450f-5f63-4371-b618-8478b45cba4f"
                }
            ]
        },
        {
            "id": "396fcd9f-0def-4464-b223-547ddbc1a1e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c153a42a-6615-45aa-9ea2-83466ad49604",
            "compositeImage": {
                "id": "1968c1d7-4718-49fb-96d4-c9789e1c9dc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "396fcd9f-0def-4464-b223-547ddbc1a1e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebb0fa5a-28dd-405a-beaf-6e344aff6661",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "396fcd9f-0def-4464-b223-547ddbc1a1e2",
                    "LayerId": "d401450f-5f63-4371-b618-8478b45cba4f"
                }
            ]
        },
        {
            "id": "b34b7613-5a17-4a1b-bc4e-46c2aeb4eb15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c153a42a-6615-45aa-9ea2-83466ad49604",
            "compositeImage": {
                "id": "2dc5f2cb-3e2d-4d70-a284-fc6d35d76fd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b34b7613-5a17-4a1b-bc4e-46c2aeb4eb15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b885f61d-ac6d-4afd-b72c-5dfcd853e805",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b34b7613-5a17-4a1b-bc4e-46c2aeb4eb15",
                    "LayerId": "d401450f-5f63-4371-b618-8478b45cba4f"
                }
            ]
        },
        {
            "id": "7e50c07b-f911-4994-ab48-7db0d7a0fd8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c153a42a-6615-45aa-9ea2-83466ad49604",
            "compositeImage": {
                "id": "467c8fbb-c077-49ce-8d11-c5158457cd5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e50c07b-f911-4994-ab48-7db0d7a0fd8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6540e729-b7d2-4b23-83c0-b261764f63d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e50c07b-f911-4994-ab48-7db0d7a0fd8a",
                    "LayerId": "d401450f-5f63-4371-b618-8478b45cba4f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "d401450f-5f63-4371-b618-8478b45cba4f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c153a42a-6615-45aa-9ea2-83466ad49604",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}
{
    "id": "0e6baaec-f4b5-4ace-ade5-0e10ad2275dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 7,
    "bbox_right": 39,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6fe8830c-0ced-43e9-8371-95be37020cbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e6baaec-f4b5-4ace-ade5-0e10ad2275dd",
            "compositeImage": {
                "id": "613fead3-7bb8-47af-a1ec-f5132b2bc475",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fe8830c-0ced-43e9-8371-95be37020cbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2094367-ca28-479e-90b8-9e4aa1ebd40c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fe8830c-0ced-43e9-8371-95be37020cbc",
                    "LayerId": "f83925c3-8d19-4c7a-bbb9-d67e9c71371b"
                }
            ]
        },
        {
            "id": "a3b3be75-e097-43fe-b001-4a7e2a868950",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e6baaec-f4b5-4ace-ade5-0e10ad2275dd",
            "compositeImage": {
                "id": "eb39c18a-517e-4027-ad1f-b0234817dc5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3b3be75-e097-43fe-b001-4a7e2a868950",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c64a137c-70f7-4579-ab14-e455579a2a6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3b3be75-e097-43fe-b001-4a7e2a868950",
                    "LayerId": "f83925c3-8d19-4c7a-bbb9-d67e9c71371b"
                }
            ]
        },
        {
            "id": "7d071a0f-621d-4b9f-94f3-663888714bdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e6baaec-f4b5-4ace-ade5-0e10ad2275dd",
            "compositeImage": {
                "id": "48a04e17-82e1-4e53-ad90-fce41505f5aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d071a0f-621d-4b9f-94f3-663888714bdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e14c0c2a-5ce9-4165-96d4-899d50966476",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d071a0f-621d-4b9f-94f3-663888714bdb",
                    "LayerId": "f83925c3-8d19-4c7a-bbb9-d67e9c71371b"
                }
            ]
        },
        {
            "id": "3ab9e529-86bc-459c-95ab-4504c5b425ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e6baaec-f4b5-4ace-ade5-0e10ad2275dd",
            "compositeImage": {
                "id": "afd23790-df28-47a9-acc8-202e586dcec1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ab9e529-86bc-459c-95ab-4504c5b425ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62141e6c-56b5-40bc-ac0b-f6f92527b1af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ab9e529-86bc-459c-95ab-4504c5b425ae",
                    "LayerId": "f83925c3-8d19-4c7a-bbb9-d67e9c71371b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "f83925c3-8d19-4c7a-bbb9-d67e9c71371b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e6baaec-f4b5-4ace-ade5-0e10ad2275dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}
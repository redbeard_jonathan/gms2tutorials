{
    "id": "1057713b-da8c-4405-9b1d-64608dc4b5f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ac59bc2e-e7f9-4be2-9117-9d6aa64e1fc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1057713b-da8c-4405-9b1d-64608dc4b5f7",
            "compositeImage": {
                "id": "1fd8b2e6-afe7-4d27-bc39-09f4afbac8de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac59bc2e-e7f9-4be2-9117-9d6aa64e1fc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af0ecd6b-9121-4862-91a5-ccba3f034a09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac59bc2e-e7f9-4be2-9117-9d6aa64e1fc9",
                    "LayerId": "9c2670b6-3ab5-4993-8206-fcc22425bca2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "9c2670b6-3ab5-4993-8206-fcc22425bca2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1057713b-da8c-4405-9b1d-64608dc4b5f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 10,
    "yorig": 4
}
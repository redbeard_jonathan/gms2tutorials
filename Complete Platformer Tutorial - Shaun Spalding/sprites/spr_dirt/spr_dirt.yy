{
    "id": "bd3aa349-9160-41d6-9e93-754254f79e71",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dirt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 131,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "134a2992-f89a-49f5-b7ea-2e2e2deefa94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd3aa349-9160-41d6-9e93-754254f79e71",
            "compositeImage": {
                "id": "de04d3b8-37cb-46bf-88db-b499bbfab320",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "134a2992-f89a-49f5-b7ea-2e2e2deefa94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "669e123a-3a68-4af9-896e-70c4892f234b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "134a2992-f89a-49f5-b7ea-2e2e2deefa94",
                    "LayerId": "4cf18969-6c3a-4da3-a708-d745b74b9727"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 132,
    "layers": [
        {
            "id": "4cf18969-6c3a-4da3-a708-d745b74b9727",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bd3aa349-9160-41d6-9e93-754254f79e71",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": -173,
    "yorig": -6
}
{
    "id": "8d00206a-7042-48cf-999a-0b8e33fc83a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 7,
    "bbox_right": 39,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "809aefc3-9e5e-4c27-bdf7-9579cf665b0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d00206a-7042-48cf-999a-0b8e33fc83a1",
            "compositeImage": {
                "id": "e1d477e5-910b-4aac-888f-c9edebd7682b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "809aefc3-9e5e-4c27-bdf7-9579cf665b0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39adf5a6-24a0-4ae3-b0cf-85d3ae299d31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "809aefc3-9e5e-4c27-bdf7-9579cf665b0d",
                    "LayerId": "08a108cf-eac1-4557-9d70-6af3404b7985"
                }
            ]
        },
        {
            "id": "d8903ab5-f80a-4f56-82a8-07a5557465c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d00206a-7042-48cf-999a-0b8e33fc83a1",
            "compositeImage": {
                "id": "1074e339-5816-438e-95bc-a90dc9a39ed0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8903ab5-f80a-4f56-82a8-07a5557465c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06d6394e-b018-4da1-946b-08b17aa85bdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8903ab5-f80a-4f56-82a8-07a5557465c6",
                    "LayerId": "08a108cf-eac1-4557-9d70-6af3404b7985"
                }
            ]
        },
        {
            "id": "17111a29-4e9f-4284-9e94-a25d710869d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d00206a-7042-48cf-999a-0b8e33fc83a1",
            "compositeImage": {
                "id": "22e11705-f928-4cf5-b2a7-b25add003f4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17111a29-4e9f-4284-9e94-a25d710869d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "929186ff-cc0e-4b36-9754-1680acccf2b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17111a29-4e9f-4284-9e94-a25d710869d7",
                    "LayerId": "08a108cf-eac1-4557-9d70-6af3404b7985"
                }
            ]
        },
        {
            "id": "dea6d270-6ef1-4fcf-a390-16823d13811f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d00206a-7042-48cf-999a-0b8e33fc83a1",
            "compositeImage": {
                "id": "200a2af2-1d4c-43cb-9415-bf1240dced58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dea6d270-6ef1-4fcf-a390-16823d13811f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72ef91dd-4080-49d1-a40e-30c3112594ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dea6d270-6ef1-4fcf-a390-16823d13811f",
                    "LayerId": "08a108cf-eac1-4557-9d70-6af3404b7985"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "08a108cf-eac1-4557-9d70-6af3404b7985",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d00206a-7042-48cf-999a-0b8e33fc83a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}
{
    "id": "a4d2b3f5-d2ef-493e-8820-f5c3034b277a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sign_marker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6357b1e4-fd30-49ec-8c0d-430f6ad212cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4d2b3f5-d2ef-493e-8820-f5c3034b277a",
            "compositeImage": {
                "id": "ea61ecf7-a11b-452d-94f4-56c17c6d2b41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6357b1e4-fd30-49ec-8c0d-430f6ad212cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d6d6558-b457-4ce0-a2b0-7abcd1a05144",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6357b1e4-fd30-49ec-8c0d-430f6ad212cc",
                    "LayerId": "db5fbffa-1d02-496b-a56e-8b6d2e4ca6be"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "db5fbffa-1d02-496b-a56e-8b6d2e4ca6be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a4d2b3f5-d2ef-493e-8820-f5c3034b277a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 8,
    "yorig": -1
}
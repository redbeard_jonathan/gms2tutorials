{
    "id": "c73edc22-f13f-48ca-8f6e-e20439e04dba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_running_backwards",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 7,
    "bbox_right": 40,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "49751d88-6dcc-4b6e-99fa-154f76702af3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c73edc22-f13f-48ca-8f6e-e20439e04dba",
            "compositeImage": {
                "id": "c5c6cff1-ab5b-482f-b93b-b759c3671e9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49751d88-6dcc-4b6e-99fa-154f76702af3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47a6dcb8-c306-4cdb-9e74-ffabb418a6d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49751d88-6dcc-4b6e-99fa-154f76702af3",
                    "LayerId": "5d02ee80-49db-4075-bb9a-83c348169b4a"
                }
            ]
        },
        {
            "id": "105027a4-14a4-47ae-b3ee-848e5a4a240a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c73edc22-f13f-48ca-8f6e-e20439e04dba",
            "compositeImage": {
                "id": "90849990-1ce5-4c42-88c3-df3ad6205137",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "105027a4-14a4-47ae-b3ee-848e5a4a240a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f48244df-4088-4bfd-a959-f28e3dd03d64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "105027a4-14a4-47ae-b3ee-848e5a4a240a",
                    "LayerId": "5d02ee80-49db-4075-bb9a-83c348169b4a"
                }
            ]
        },
        {
            "id": "ce590073-cdfb-46a3-8dc9-a1a9b16bccec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c73edc22-f13f-48ca-8f6e-e20439e04dba",
            "compositeImage": {
                "id": "8f27dfe9-c06d-4385-8cb9-22f81a5bbbd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce590073-cdfb-46a3-8dc9-a1a9b16bccec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d39f90ac-9010-4a23-be22-44cf617b1569",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce590073-cdfb-46a3-8dc9-a1a9b16bccec",
                    "LayerId": "5d02ee80-49db-4075-bb9a-83c348169b4a"
                }
            ]
        },
        {
            "id": "fa31ebca-973d-404f-84cd-25a885b06876",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c73edc22-f13f-48ca-8f6e-e20439e04dba",
            "compositeImage": {
                "id": "3e03c4ea-244d-41fe-8687-1d03a294c8f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa31ebca-973d-404f-84cd-25a885b06876",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fea73d62-f0b2-4d92-b71f-89a33b932493",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa31ebca-973d-404f-84cd-25a885b06876",
                    "LayerId": "5d02ee80-49db-4075-bb9a-83c348169b4a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "5d02ee80-49db-4075-bb9a-83c348169b4a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c73edc22-f13f-48ca-8f6e-e20439e04dba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}
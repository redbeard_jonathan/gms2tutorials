{
    "id": "7b6c9c41-3360-4546-bb3b-c13ff9ee82ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_crate",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3955a87c-7981-49b4-9ef6-9bc553772eb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b6c9c41-3360-4546-bb3b-c13ff9ee82ec",
            "compositeImage": {
                "id": "83f65121-83b7-4c54-b686-cfd7d5c01839",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3955a87c-7981-49b4-9ef6-9bc553772eb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "159f45e9-c118-45ff-b180-2bfb6adb6ae2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3955a87c-7981-49b4-9ef6-9bc553772eb8",
                    "LayerId": "695b3ad2-8322-4fa5-8a69-0f6bc24e0be6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "695b3ad2-8322-4fa5-8a69-0f6bc24e0be6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b6c9c41-3360-4546-bb3b-c13ff9ee82ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
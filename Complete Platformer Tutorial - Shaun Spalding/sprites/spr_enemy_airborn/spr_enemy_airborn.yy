{
    "id": "2c1444a7-57d8-4200-9a72-ebb222c77333",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_airborn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 7,
    "bbox_right": 39,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f47edc1e-2c35-45e5-be83-d0d8f688cef7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c1444a7-57d8-4200-9a72-ebb222c77333",
            "compositeImage": {
                "id": "90dc8fbf-b2fb-4779-a29e-e67b0490f348",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f47edc1e-2c35-45e5-be83-d0d8f688cef7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5ad360e-d5fb-4697-afce-bc91f8b0e02a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f47edc1e-2c35-45e5-be83-d0d8f688cef7",
                    "LayerId": "72a2f202-6f68-424f-bc09-8e4ce8905944"
                }
            ]
        },
        {
            "id": "4cc2dc26-10c6-4ca5-ba4b-68a53e116605",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c1444a7-57d8-4200-9a72-ebb222c77333",
            "compositeImage": {
                "id": "8dcdec8b-2576-4637-a93b-14701698b57a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cc2dc26-10c6-4ca5-ba4b-68a53e116605",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b32f4069-42e7-48cb-ab67-298922f1c5e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cc2dc26-10c6-4ca5-ba4b-68a53e116605",
                    "LayerId": "72a2f202-6f68-424f-bc09-8e4ce8905944"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "72a2f202-6f68-424f-bc09-8e4ce8905944",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c1444a7-57d8-4200-9a72-ebb222c77333",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}
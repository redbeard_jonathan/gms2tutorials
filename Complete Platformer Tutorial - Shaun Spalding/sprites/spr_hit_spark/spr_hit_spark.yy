{
    "id": "4fdc345c-fcbf-4efa-bd04-1dd66c8272c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hit_spark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 0,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "63242f95-9a21-4011-b0ad-0fb2b27e688d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4fdc345c-fcbf-4efa-bd04-1dd66c8272c6",
            "compositeImage": {
                "id": "6cf4ba61-ccf0-4200-bcc5-f3bce27ae74a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63242f95-9a21-4011-b0ad-0fb2b27e688d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19633881-35c6-40cf-b553-7d7dc9dde243",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63242f95-9a21-4011-b0ad-0fb2b27e688d",
                    "LayerId": "37d9c09d-493d-4965-b123-3fb293e43fdc"
                }
            ]
        },
        {
            "id": "eb940c4f-d080-4bef-8ff3-d672ea3c0050",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4fdc345c-fcbf-4efa-bd04-1dd66c8272c6",
            "compositeImage": {
                "id": "06003fbc-8b4f-4967-9887-cb0351bc0f5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb940c4f-d080-4bef-8ff3-d672ea3c0050",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b55cd04-5c37-4da0-8aae-6298e6517a8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb940c4f-d080-4bef-8ff3-d672ea3c0050",
                    "LayerId": "37d9c09d-493d-4965-b123-3fb293e43fdc"
                }
            ]
        },
        {
            "id": "727482bd-0b0f-4999-af3f-d87768b6965b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4fdc345c-fcbf-4efa-bd04-1dd66c8272c6",
            "compositeImage": {
                "id": "e6a4b9ed-4fd1-451a-8116-adf3b738dbd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "727482bd-0b0f-4999-af3f-d87768b6965b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07a2f3e1-2b82-4faa-a5fe-a7e1916b0313",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "727482bd-0b0f-4999-af3f-d87768b6965b",
                    "LayerId": "37d9c09d-493d-4965-b123-3fb293e43fdc"
                }
            ]
        },
        {
            "id": "0c17ca36-a9a1-4ba6-b6bf-540a5b1c303a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4fdc345c-fcbf-4efa-bd04-1dd66c8272c6",
            "compositeImage": {
                "id": "b7928301-138a-46be-9ad0-94b83b309c07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c17ca36-a9a1-4ba6-b6bf-540a5b1c303a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "850fe129-8e50-456b-99a6-b00a830ac2ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c17ca36-a9a1-4ba6-b6bf-540a5b1c303a",
                    "LayerId": "37d9c09d-493d-4965-b123-3fb293e43fdc"
                }
            ]
        },
        {
            "id": "cc4f376c-3f71-4dd6-be54-c6210f3b53c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4fdc345c-fcbf-4efa-bd04-1dd66c8272c6",
            "compositeImage": {
                "id": "f42cd4d2-63b8-4a98-a5b0-d43e93c2eec9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc4f376c-3f71-4dd6-be54-c6210f3b53c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59adf0d6-9335-4201-a787-aadbde20eee1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc4f376c-3f71-4dd6-be54-c6210f3b53c9",
                    "LayerId": "37d9c09d-493d-4965-b123-3fb293e43fdc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "37d9c09d-493d-4965-b123-3fb293e43fdc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4fdc345c-fcbf-4efa-bd04-1dd66c8272c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
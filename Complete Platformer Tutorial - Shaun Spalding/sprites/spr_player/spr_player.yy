{
    "id": "138275f6-4415-4b75-9c3f-cb5a96125562",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 19,
    "bbox_right": 29,
    "bbox_top": 17,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46beb974-aeb3-441b-aa19-b2f6e426b519",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "138275f6-4415-4b75-9c3f-cb5a96125562",
            "compositeImage": {
                "id": "ae361872-e4f8-4009-9768-ea1f6b5c6975",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46beb974-aeb3-441b-aa19-b2f6e426b519",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63c59d7e-16c7-4820-9f50-8e72f248917c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46beb974-aeb3-441b-aa19-b2f6e426b519",
                    "LayerId": "8b761b4b-fc10-4436-8bd9-f111bfea3088"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "8b761b4b-fc10-4436-8bd9-f111bfea3088",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "138275f6-4415-4b75-9c3f-cb5a96125562",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}
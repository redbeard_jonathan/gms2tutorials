{
    "id": "d2afb1b8-9e99-4c01-8ac6-2e2bbdba95e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_airborn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 7,
    "bbox_right": 39,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cc60a23f-6de3-413e-95a1-fa9253f59078",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2afb1b8-9e99-4c01-8ac6-2e2bbdba95e9",
            "compositeImage": {
                "id": "ffd602ac-0e6f-4d1e-ad7b-2a8a59fae723",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc60a23f-6de3-413e-95a1-fa9253f59078",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fbc9a5d-ac04-4297-b559-43361254fa9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc60a23f-6de3-413e-95a1-fa9253f59078",
                    "LayerId": "947cef0a-a6b7-4d13-9276-a698d48c4043"
                }
            ]
        },
        {
            "id": "e60606f8-248f-4f05-be21-2c609c82c7e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2afb1b8-9e99-4c01-8ac6-2e2bbdba95e9",
            "compositeImage": {
                "id": "f9f5de40-358e-48df-8d2f-21ad1ca97dab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e60606f8-248f-4f05-be21-2c609c82c7e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15123fd3-2486-4193-99d4-55bb0c03ba5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e60606f8-248f-4f05-be21-2c609c82c7e4",
                    "LayerId": "947cef0a-a6b7-4d13-9276-a698d48c4043"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "947cef0a-a6b7-4d13-9276-a698d48c4043",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d2afb1b8-9e99-4c01-8ac6-2e2bbdba95e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}
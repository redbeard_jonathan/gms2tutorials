{
    "id": "e5e48527-3b3d-4e04-a59c-ee75e4a3bcd9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_trees",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 38,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b532e4d2-6f9e-4c68-9e1f-b2bf80008d4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5e48527-3b3d-4e04-a59c-ee75e4a3bcd9",
            "compositeImage": {
                "id": "ecb4c0d8-2e40-4ce7-9416-98f1143612b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b532e4d2-6f9e-4c68-9e1f-b2bf80008d4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19c3aa81-1e5f-46f5-84e1-1dcd2a2f9436",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b532e4d2-6f9e-4c68-9e1f-b2bf80008d4f",
                    "LayerId": "85fe7b52-ad98-4e7e-bc04-68a8dc0c6654"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 500,
    "layers": [
        {
            "id": "85fe7b52-ad98-4e7e-bc04-68a8dc0c6654",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5e48527-3b3d-4e04-a59c-ee75e4a3bcd9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 250
}
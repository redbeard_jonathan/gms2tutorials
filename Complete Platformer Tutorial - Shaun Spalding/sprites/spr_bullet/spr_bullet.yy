{
    "id": "dd721dff-8293-4335-860e-f91e588cf272",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 7,
    "bbox_right": 32,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93166420-bae0-4660-be1c-9f1bf85b43bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd721dff-8293-4335-860e-f91e588cf272",
            "compositeImage": {
                "id": "349c10b3-0e6c-4a17-9402-5f09fd120de5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93166420-bae0-4660-be1c-9f1bf85b43bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce1a3ef4-4407-4de2-9fd6-7eb370f40988",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93166420-bae0-4660-be1c-9f1bf85b43bf",
                    "LayerId": "28f42989-7855-4359-b5ef-3f89b9d7e613"
                }
            ]
        },
        {
            "id": "94824bab-d494-428c-9e65-210444f18b09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd721dff-8293-4335-860e-f91e588cf272",
            "compositeImage": {
                "id": "551a02ef-a000-45c3-ba83-0edef84dafff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94824bab-d494-428c-9e65-210444f18b09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5e92a3f-3787-4ea9-94d3-7c331d0033a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94824bab-d494-428c-9e65-210444f18b09",
                    "LayerId": "28f42989-7855-4359-b5ef-3f89b9d7e613"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "28f42989-7855-4359-b5ef-3f89b9d7e613",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd721dff-8293-4335-860e-f91e588cf272",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 9
}
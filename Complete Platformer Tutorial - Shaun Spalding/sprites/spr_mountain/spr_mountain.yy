{
    "id": "248f7a09-7a6f-49e7-b809-762a61fbd09d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mountain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 110,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1dfb8032-9ddb-4a61-8bb3-9ffb41053bd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "248f7a09-7a6f-49e7-b809-762a61fbd09d",
            "compositeImage": {
                "id": "3965c17d-6d9b-414c-8b93-32d049fefe85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dfb8032-9ddb-4a61-8bb3-9ffb41053bd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a4f78b5-89a6-4e62-8315-08fad6465306",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dfb8032-9ddb-4a61-8bb3-9ffb41053bd9",
                    "LayerId": "48a57c1c-099a-4c01-95d3-8a0d56873829"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "48a57c1c-099a-4c01-95d3-8a0d56873829",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "248f7a09-7a6f-49e7-b809-762a61fbd09d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 240
}
{
    "id": "780ec3c0-4695-4eba-8b63-71b11c2402fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dust",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b953176-8ae3-4c40-b351-79a1c0fd7497",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "780ec3c0-4695-4eba-8b63-71b11c2402fc",
            "compositeImage": {
                "id": "74056490-27af-4b9b-81c7-7135d901176a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b953176-8ae3-4c40-b351-79a1c0fd7497",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee0996fd-abca-4995-a2f1-007a677ec9db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b953176-8ae3-4c40-b351-79a1c0fd7497",
                    "LayerId": "ff1652e8-8b51-45fd-95dc-e8f8b8d1dd88"
                }
            ]
        },
        {
            "id": "8dbc4c1c-df58-49c7-8684-56eb2e5ba750",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "780ec3c0-4695-4eba-8b63-71b11c2402fc",
            "compositeImage": {
                "id": "62d7b2fc-62c0-423f-8493-20450610b662",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dbc4c1c-df58-49c7-8684-56eb2e5ba750",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbeddb4e-4a65-4729-ad14-8f9253ccba2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dbc4c1c-df58-49c7-8684-56eb2e5ba750",
                    "LayerId": "ff1652e8-8b51-45fd-95dc-e8f8b8d1dd88"
                }
            ]
        },
        {
            "id": "81eec5f4-fa80-4c1f-9ede-a0e8cff129db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "780ec3c0-4695-4eba-8b63-71b11c2402fc",
            "compositeImage": {
                "id": "3cbd4aa7-10fd-4e2b-8e93-bdb795b015dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81eec5f4-fa80-4c1f-9ede-a0e8cff129db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "549ed0ef-299c-4dc6-b1ff-d970d606f942",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81eec5f4-fa80-4c1f-9ede-a0e8cff129db",
                    "LayerId": "ff1652e8-8b51-45fd-95dc-e8f8b8d1dd88"
                }
            ]
        },
        {
            "id": "0c2ea7e9-ff26-4f67-93f8-6a7edaf42452",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "780ec3c0-4695-4eba-8b63-71b11c2402fc",
            "compositeImage": {
                "id": "0f9ce3dd-e924-49e6-a2b9-84bcdebad058",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c2ea7e9-ff26-4f67-93f8-6a7edaf42452",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6e8e84a-2759-407d-9bde-8e4e21f877d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c2ea7e9-ff26-4f67-93f8-6a7edaf42452",
                    "LayerId": "ff1652e8-8b51-45fd-95dc-e8f8b8d1dd88"
                }
            ]
        },
        {
            "id": "b4ebc090-27e5-49f5-a8ec-c8d9db9a1122",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "780ec3c0-4695-4eba-8b63-71b11c2402fc",
            "compositeImage": {
                "id": "2d2c281a-5afe-428c-84d6-952391753ffd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4ebc090-27e5-49f5-a8ec-c8d9db9a1122",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b28d6bed-7946-45c3-a2ad-599374eb8cf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4ebc090-27e5-49f5-a8ec-c8d9db9a1122",
                    "LayerId": "ff1652e8-8b51-45fd-95dc-e8f8b8d1dd88"
                }
            ]
        },
        {
            "id": "c9521e9e-2ef0-4018-b49e-6e455acc15c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "780ec3c0-4695-4eba-8b63-71b11c2402fc",
            "compositeImage": {
                "id": "496ffe14-e92e-4920-b5b5-923c3cae8236",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9521e9e-2ef0-4018-b49e-6e455acc15c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e029b5ba-17a6-4737-acf9-ec6454121de2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9521e9e-2ef0-4018-b49e-6e455acc15c3",
                    "LayerId": "ff1652e8-8b51-45fd-95dc-e8f8b8d1dd88"
                }
            ]
        },
        {
            "id": "d93acf51-b013-4383-8c37-04021aa0a770",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "780ec3c0-4695-4eba-8b63-71b11c2402fc",
            "compositeImage": {
                "id": "7eb17200-6b3d-41b4-a08f-eb8f0afdf0b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d93acf51-b013-4383-8c37-04021aa0a770",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05e94388-7730-4ebe-bacf-6930f94b457f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d93acf51-b013-4383-8c37-04021aa0a770",
                    "LayerId": "ff1652e8-8b51-45fd-95dc-e8f8b8d1dd88"
                }
            ]
        },
        {
            "id": "6b28445a-b256-4a86-b1d0-d70d12651401",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "780ec3c0-4695-4eba-8b63-71b11c2402fc",
            "compositeImage": {
                "id": "d6aec59c-2dec-4517-92dd-83f8b9b1f245",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b28445a-b256-4a86-b1d0-d70d12651401",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20ba023c-3220-487b-8253-62c9689303bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b28445a-b256-4a86-b1d0-d70d12651401",
                    "LayerId": "ff1652e8-8b51-45fd-95dc-e8f8b8d1dd88"
                }
            ]
        },
        {
            "id": "b878b620-484a-4a0f-a1ce-10163bd310ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "780ec3c0-4695-4eba-8b63-71b11c2402fc",
            "compositeImage": {
                "id": "9d37829f-1a26-494e-93f6-0325bc7d40e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b878b620-484a-4a0f-a1ce-10163bd310ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0372dc3b-e8c9-486d-bf45-bbb8a331d737",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b878b620-484a-4a0f-a1ce-10163bd310ec",
                    "LayerId": "ff1652e8-8b51-45fd-95dc-e8f8b8d1dd88"
                }
            ]
        },
        {
            "id": "52bb79b9-6e63-4ee0-abcd-16cffa6098db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "780ec3c0-4695-4eba-8b63-71b11c2402fc",
            "compositeImage": {
                "id": "854b65a3-8a26-4a45-a2a5-22b1d365a90c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52bb79b9-6e63-4ee0-abcd-16cffa6098db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37778124-0850-481a-8497-abbd7244c65e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52bb79b9-6e63-4ee0-abcd-16cffa6098db",
                    "LayerId": "ff1652e8-8b51-45fd-95dc-e8f8b8d1dd88"
                }
            ]
        },
        {
            "id": "b111f0b2-091d-454c-a8fc-30f88a0529bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "780ec3c0-4695-4eba-8b63-71b11c2402fc",
            "compositeImage": {
                "id": "19cc972e-411a-49b6-b982-bacea154e29d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b111f0b2-091d-454c-a8fc-30f88a0529bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e412048a-efb3-4a22-887f-7a6e2b277d03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b111f0b2-091d-454c-a8fc-30f88a0529bf",
                    "LayerId": "ff1652e8-8b51-45fd-95dc-e8f8b8d1dd88"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "ff1652e8-8b51-45fd-95dc-e8f8b8d1dd88",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "780ec3c0-4695-4eba-8b63-71b11c2402fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}
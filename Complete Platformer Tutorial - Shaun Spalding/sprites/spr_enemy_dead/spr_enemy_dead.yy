{
    "id": "c232cbdd-6035-4539-b491-7ad2336e600a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 19,
    "bbox_right": 32,
    "bbox_top": 35,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ab37b25b-6b53-4482-b53c-be8affe233e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c232cbdd-6035-4539-b491-7ad2336e600a",
            "compositeImage": {
                "id": "49186ca3-7b6d-4310-ae46-9f8f3a806e32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab37b25b-6b53-4482-b53c-be8affe233e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff0fb1a1-d791-4947-917e-7384e09e82ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab37b25b-6b53-4482-b53c-be8affe233e7",
                    "LayerId": "7759076b-c0b3-4ee2-a274-0e0a371f5c2a"
                }
            ]
        },
        {
            "id": "f3a87295-3f9c-41dd-a800-0a21122020c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c232cbdd-6035-4539-b491-7ad2336e600a",
            "compositeImage": {
                "id": "05c808f4-24e5-418a-9db8-028aa53eae38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3a87295-3f9c-41dd-a800-0a21122020c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb5fa0d3-11c7-4edf-85c6-bddb57276439",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3a87295-3f9c-41dd-a800-0a21122020c3",
                    "LayerId": "7759076b-c0b3-4ee2-a274-0e0a371f5c2a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "7759076b-c0b3-4ee2-a274-0e0a371f5c2a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c232cbdd-6035-4539-b491-7ad2336e600a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 21,
    "yorig": 40
}
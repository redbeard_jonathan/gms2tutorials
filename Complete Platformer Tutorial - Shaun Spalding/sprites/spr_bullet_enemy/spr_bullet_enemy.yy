{
    "id": "d4aabd89-6bb2-4e10-97b4-27552204d693",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 6,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "217a6cd4-529e-4bb5-9d0f-4002ee6705bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4aabd89-6bb2-4e10-97b4-27552204d693",
            "compositeImage": {
                "id": "12507df2-308b-4e3f-a8f4-a81396b644ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "217a6cd4-529e-4bb5-9d0f-4002ee6705bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2c85f36-0637-45a9-9b3f-fc405f7562b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "217a6cd4-529e-4bb5-9d0f-4002ee6705bf",
                    "LayerId": "0611aef4-11e2-43c2-9068-aa482a0af7c6"
                }
            ]
        },
        {
            "id": "8eeecd6a-145c-4f56-9661-9f197276425e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4aabd89-6bb2-4e10-97b4-27552204d693",
            "compositeImage": {
                "id": "b4f249ff-1371-45d9-bf50-d172c1cf727b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8eeecd6a-145c-4f56-9661-9f197276425e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c304943-072f-4c9a-9807-b004730e2940",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8eeecd6a-145c-4f56-9661-9f197276425e",
                    "LayerId": "0611aef4-11e2-43c2-9068-aa482a0af7c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "0611aef4-11e2-43c2-9068-aa482a0af7c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d4aabd89-6bb2-4e10-97b4-27552204d693",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 9
}
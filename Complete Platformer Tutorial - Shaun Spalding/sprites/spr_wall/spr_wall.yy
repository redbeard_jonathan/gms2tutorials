{
    "id": "8fb7dd91-2d03-403d-a394-755ed5ee9ca3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb9bc9a5-75d1-4a4e-b4cd-df6041cbad3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb7dd91-2d03-403d-a394-755ed5ee9ca3",
            "compositeImage": {
                "id": "9e02555e-9e1e-4f45-95fa-864b717d63b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb9bc9a5-75d1-4a4e-b4cd-df6041cbad3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da8b0ad1-9f2a-475e-b3de-df8feb8aeda6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb9bc9a5-75d1-4a4e-b4cd-df6041cbad3c",
                    "LayerId": "13a39216-0a19-40a7-801c-45f525f16b10"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "13a39216-0a19-40a7-801c-45f525f16b10",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8fb7dd91-2d03-403d-a394-755ed5ee9ca3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
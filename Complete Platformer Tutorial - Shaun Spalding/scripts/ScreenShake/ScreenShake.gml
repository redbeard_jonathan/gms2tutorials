/// @description ScreenShake(magnitude, frames)
/// @arg magnitude Sets the strength of the shake (radius in pixels)
/// @arg framesd Sets the length of the shake in frames

with(obj_camera)
{
	if(argument[0] > shake_remain)
	{
		shake_magnitude = argument[0];
		shake_remain = argument[0];
		shake_length = argument[1];
	}
}

#macro RES_W 1024
#macro RES_H 768

display_set_gui_size(RES_W, RES_H);

global.kills = 0;
global.kills_this_room = 0;
global.has_gun = false;

kill_text_scale = 1;

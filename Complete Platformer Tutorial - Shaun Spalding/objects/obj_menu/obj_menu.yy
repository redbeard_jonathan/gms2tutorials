{
    "id": "3523ed9d-bd91-49ad-8796-9fec522f8ef7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_menu",
    "eventList": [
        {
            "id": "3f340205-c908-43c7-a454-654d7a4de6d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3523ed9d-bd91-49ad-8796-9fec522f8ef7"
        },
        {
            "id": "d442ebf4-5783-408b-9eec-fc6cc8b85d4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "3523ed9d-bd91-49ad-8796-9fec522f8ef7"
        },
        {
            "id": "ebe5016f-328d-494d-9e70-74821c8de3f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3523ed9d-bd91-49ad-8796-9fec522f8ef7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
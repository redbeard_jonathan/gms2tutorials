/// @description Sync gun to player

x = obj_player.x;
y = obj_player.y + 10;

if(obj_player.controller == 0)
{
	image_angle = point_direction(x, y, mouse_x, mouse_y);
}
else
{
	var controllerh = gamepad_axis_value(0, gp_axisrh);
	var controllerv = gamepad_axis_value(0, gp_axisrv);
	
	if(abs(controllerh > .2) || abs(controllerv > .2))
	{
		controller_angle = point_direction(0, 0, controllerh, controllerv);
	}
	
	image_angle = controller_angle;
}

firing_delay -= 1;
recoil = max(0, recoil - 1);

if((mouse_check_button(mb_left) || gamepad_button_check(0, gp_shoulderlb)) && firing_delay < 0)
{
	firing_delay = 5;
	recoil = 4;
	ScreenShake(2, 10);
	
	audio_play_sound(sn_shot, 1, false);
	
	with(instance_create_layer(x, y, "Bullets", obj_bullet))
	{
		spd = 25;
		direction = other.image_angle + random_range(-3, 3); //gun image angle
		
		image_angle = direction;
	}
	
	with(obj_player)
	{
		gun_kick_x = lengthdir_x(1.5, other.image_angle-180);
		gun_kick_y = lengthdir_y(1, other.image_angle-180);
	}
}

x = x-lengthdir_x(recoil, image_angle);
y = y-lengthdir_y(recoil, image_angle);

if(image_angle > 90 && image_angle < 270)
{
	image_yscale = -1;
}
else
{
	image_yscale = 1;
}

{
    "id": "4a0ed429-cba1-4967-bd6c-d3bace3bd6f3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy_dead",
    "eventList": [
        {
            "id": "a6f1d33a-f5c6-4d1a-80ff-160f15c060eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4a0ed429-cba1-4967-bd6c-d3bace3bd6f3"
        },
        {
            "id": "a1087e5d-c1b0-45bc-9ea8-39c5acf13a13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4a0ed429-cba1-4967-bd6c-d3bace3bd6f3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c232cbdd-6035-4539-b491-7ad2336e600a",
    "visible": true
}
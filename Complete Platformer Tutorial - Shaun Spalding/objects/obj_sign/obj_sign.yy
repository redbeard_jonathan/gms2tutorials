{
    "id": "006c2b22-63dd-41ff-9858-20c237d3f61b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_sign",
    "eventList": [
        {
            "id": "f506509f-c5d3-47fa-b042-6a4b07419063",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "006c2b22-63dd-41ff-9858-20c237d3f61b"
        },
        {
            "id": "e582a03b-e13f-4360-b17f-0957a2ecba4f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "006c2b22-63dd-41ff-9858-20c237d3f61b"
        },
        {
            "id": "eddb8687-98d7-4a96-ba13-d86ab3cc8b10",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "006c2b22-63dd-41ff-9858-20c237d3f61b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "eb4ab882-6cef-4e26-a812-07311eb68cf4",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "\"default\"",
            "varName": "text",
            "varType": 2
        }
    ],
    "solid": false,
    "spriteId": "1f375c28-4f34-4d43-af22-f758bbf014fb",
    "visible": true
}
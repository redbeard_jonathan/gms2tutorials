{
    "id": "89577d30-7077-417a-8d6a-de271d58b0cb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet_enemy",
    "eventList": [
        {
            "id": "4dc69218-0543-42fc-b949-8e6664cbc142",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "89577d30-7077-417a-8d6a-de271d58b0cb"
        },
        {
            "id": "091d4b68-3c5c-47a5-9177-224b8ee64738",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "89577d30-7077-417a-8d6a-de271d58b0cb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d4aabd89-6bb2-4e10-97b4-27552204d693",
    "visible": true
}
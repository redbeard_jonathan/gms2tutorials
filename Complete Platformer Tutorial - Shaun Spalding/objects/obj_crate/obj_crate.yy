{
    "id": "4df0b00f-ed5d-4eff-a79b-272e4f9cebab",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_crate",
    "eventList": [
        {
            "id": "b006510e-fc8b-4cc0-a315-149b3ba1f7e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4df0b00f-ed5d-4eff-a79b-272e4f9cebab"
        },
        {
            "id": "701c378f-8e7c-4dc5-9948-935920e6deec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "4df0b00f-ed5d-4eff-a79b-272e4f9cebab"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "5d66975a-7fba-4e8b-a67f-1e85a3f3eebe",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "c0d638ec-3560-4bcd-ad47-39c55fddf917",
            "propertyId": "d9bbbb53-5beb-45c9-8883-aebcfe2e3a03",
            "value": "5"
        }
    ],
    "parentObjectId": "c0d638ec-3560-4bcd-ad47-39c55fddf917",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "7b6c9c41-3360-4546-bb3b-c13ff9ee82ec",
    "visible": true
}
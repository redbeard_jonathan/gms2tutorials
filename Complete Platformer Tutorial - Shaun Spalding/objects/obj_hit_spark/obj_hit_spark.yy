{
    "id": "d40a7538-e11f-450d-88f0-445c8b18aa3c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hit_spark",
    "eventList": [
        {
            "id": "eba1ec3a-6100-4d34-bfb8-52f7996ea20e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d40a7538-e11f-450d-88f0-445c8b18aa3c"
        },
        {
            "id": "22efa29c-5096-4ca1-8a6c-6342b6c5d4bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "d40a7538-e11f-450d-88f0-445c8b18aa3c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4fdc345c-fcbf-4efa-bd04-1dd66c8272c6",
    "visible": true
}
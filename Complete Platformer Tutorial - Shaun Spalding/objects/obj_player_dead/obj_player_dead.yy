{
    "id": "92bc556d-41fb-4c77-b0a7-3fb286ed2227",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_dead",
    "eventList": [
        {
            "id": "821e273c-797d-45b2-8f35-7882bc06a10b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "92bc556d-41fb-4c77-b0a7-3fb286ed2227"
        },
        {
            "id": "6a56f71c-fa6a-42ff-9bcd-8c6942821970",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "92bc556d-41fb-4c77-b0a7-3fb286ed2227"
        },
        {
            "id": "baf909fe-5026-40da-90d8-8dfd56c1ef91",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "92bc556d-41fb-4c77-b0a7-3fb286ed2227"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cd62c7b5-ea5c-4f46-afb1-77523f431cdc",
    "visible": true
}
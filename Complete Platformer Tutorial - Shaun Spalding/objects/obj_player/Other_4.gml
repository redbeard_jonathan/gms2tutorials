/// @description Auto Save

//Delete old save
if(file_exists(SAVEFILE))
{
	file_delete(SAVEFILE);
}

//Create new save
var file;
file = file_text_open_write(SAVEFILE);

file_text_write_real(file, room);
file_text_write_real(file, global.kills);
file_text_write_real(file, global.has_gun);
file_text_close(file);


//give gun if needed
if(global.has_gun)
{
	instance_create_layer(obj_player.x, obj_player.y, "Gun", obj_gun);
}

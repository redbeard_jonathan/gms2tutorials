{
    "id": "cb3ec61c-d63f-4323-9354-4d5e8cd99d7e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "74a4fdde-3108-4f8b-85ac-20ece59ed5a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cb3ec61c-d63f-4323-9354-4d5e8cd99d7e"
        },
        {
            "id": "2537f0c9-fec4-485f-a2f4-7cbb5a252df2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cb3ec61c-d63f-4323-9354-4d5e8cd99d7e"
        },
        {
            "id": "cc4cffff-2d86-4462-87be-d60cf005588d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "cb3ec61c-d63f-4323-9354-4d5e8cd99d7e"
        },
        {
            "id": "e555c9bb-088b-4a7f-9ab5-d11c70ba7123",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "cb3ec61c-d63f-4323-9354-4d5e8cd99d7e"
        },
        {
            "id": "b1d0e4c7-339c-4b21-9ebe-bd8e04d144f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "78102b12-4bde-4d93-82d6-9cf64b492c24",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cb3ec61c-d63f-4323-9354-4d5e8cd99d7e"
        },
        {
            "id": "ce9234f9-6470-415e-ba2b-60b548fc9819",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "89577d30-7077-417a-8d6a-de271d58b0cb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cb3ec61c-d63f-4323-9354-4d5e8cd99d7e"
        }
    ],
    "maskSpriteId": "138275f6-4415-4b75-9c3f-cb5a96125562",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "138275f6-4415-4b75-9c3f-cb5a96125562",
    "visible": true
}
gun_sprite = layer_sprite_get_id("TitleAssets", "g_gun");

if(global.has_gun)
{
	if(global.kills > 0)
	{
		endtext[0] = "And on this day our hero destroyed " + string(global.kills) + " people.";
		endtext[1] = "One cant help but wonder if there might have been a different way?";
		endtext[2] = "The End";
	}
	else
	{		
		endtext[0] = "Armed with his machine gun our hero stood ready for duty,";
		endtext[1] = "but did not kill needlessly.";
		endtext[2] = "Even Big Todd survived.";
		endtext[3] = "Well done.";
		endtext[4] = "The End";
	}
}
else
{
	layer_sprite_destroy(gun_sprite);
	
	endtext[0] = "Our hero avoided violence.";
	endtext[1] = "To be applauded, or derided?";
	endtext[2] = "A question for the ages.";
	endtext[3] = "The End";
}

spd = .5;
letters = 0;
currentline = 0;
length = string_length(endtext[currentline]);
text = "";

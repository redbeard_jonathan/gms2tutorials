{
    "id": "7697c5fa-944e-4283-9caf-be9637075b5f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_level_end",
    "eventList": [
        {
            "id": "de8c11d2-0340-4137-b2bb-daeb7ee6c5a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "cb3ec61c-d63f-4323-9354-4d5e8cd99d7e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7697c5fa-944e-4283-9caf-be9637075b5f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "38d5b7fd-c4d8-418e-93cd-fd4e382b4c51",
    "visible": false
}
{
    "id": "97f39022-6cbb-4ed0-a788-da47a9b7bfb9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy_big",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "5b6473b5-b221-41db-9512-caa68ef6e3fd",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "78102b12-4bde-4d93-82d6-9cf64b492c24",
            "propertyId": "630b47c6-2a56-4693-8547-fb99bb091a86",
            "value": "2"
        }
    ],
    "parentObjectId": "78102b12-4bde-4d93-82d6-9cf64b492c24",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5aec0987-c050-4979-9e7d-12cafe804a84",
    "visible": true
}
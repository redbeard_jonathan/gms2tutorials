/// @description 

vsp = vsp + grv;

//Dont walk off edges
if(grounded && afraid_of_heights && !place_meeting(x + hsp, y+1, obj_wall))
{
	//flip direction
	hsp = -hsp;
}


//Horizontal Collision
if(place_meeting(x+hsp, y, obj_wall))
{
	while(!place_meeting(x+sign(hsp), y, obj_wall))
	{
		x += sign(hsp);
	}
	
	hsp = -hsp;
}
         
x += hsp;

//Vertical Collision
if(place_meeting(x, y+vsp, obj_wall))
{
	while(!place_meeting(x, y+sign(vsp), obj_wall))
	{
		y += sign(vsp);
	}
	
	vsp = 0;
}

y += vsp;

//Animation
if(!place_meeting(x, y+1, obj_wall))
{
	grounded = false;
	sprite_index = spr_enemy_airborn;
	image_speed = 0; // dont play animation (since one is up, one is down);
	
	//vsp > 1 = falling
	if(sign(vsp) > 0)
	{
		image_index = 1;	
	}
	else
	{
		image_index = 0;
	}
}
else
{
	grounded = true;
	image_speed = 1;
	
	if(hsp == 0)
	{
		sprite_index = spr_enemy;
	}
	else
	{
		sprite_index = spr_enemy_running;
	}
}

if(hsp != 0)
{
	image_xscale = sign(hsp) * size;
}

image_yscale = size;

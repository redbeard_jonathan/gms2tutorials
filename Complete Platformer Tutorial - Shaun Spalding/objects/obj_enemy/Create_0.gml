if(has_gun)
{
	my_gun = instance_create_layer(x,y, "Gun", obj_gun_enemy);
	
	with(my_gun)
	{
		owner = other.id;
	}	
}
else
{
	my_gun = noone;
}

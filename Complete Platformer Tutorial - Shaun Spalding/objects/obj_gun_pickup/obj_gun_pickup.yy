{
    "id": "16c6074f-78ac-4500-8473-e0a14f6e6c1a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gun_pickup",
    "eventList": [
        {
            "id": "d222201f-7e48-45f7-b43c-f8e114c90b9e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "16c6074f-78ac-4500-8473-e0a14f6e6c1a"
        },
        {
            "id": "f1d0a228-fa86-4414-bdd6-08b83bcdd860",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "cb3ec61c-d63f-4323-9354-4d5e8cd99d7e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "16c6074f-78ac-4500-8473-e0a14f6e6c1a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1057713b-da8c-4405-9b1d-64608dc4b5f7",
    "visible": true
}
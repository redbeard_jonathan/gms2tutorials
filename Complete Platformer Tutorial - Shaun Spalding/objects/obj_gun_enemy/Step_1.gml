x = owner.x;
y = owner.y + 10;

image_xscale = abs(owner.image_xscale);
image_yscale = abs(owner.image_yscale);

if(instance_exists(obj_player))
{
	if(obj_player.x < x)
	{
		image_yscale = -image_yscale;
	}
	
	if(point_distance(obj_player.x, obj_player.y, x, y) < 600)
	{
		image_angle = point_direction(x, y, obj_player.x, obj_player.y);
		countdown--;
	}
	
	if(countdown <= 0)
	{
		countdown = countdown_rate;
		
		if(!collision_line(x, y, obj_player.x, obj_player.y, obj_wall, false, false))
		{
			if(global.kills > 0)
			{
				audio_play_sound(sn_shot, 1, false);
	
				with(instance_create_layer(x, y, "Bullets", obj_bullet_enemy))
				{
					spd = 10;
					direction = other.image_angle + random_range(-3, 3); //gun image angle
		
					image_angle = direction;
				}
			}
		}
	}
}

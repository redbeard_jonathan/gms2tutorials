{
    "id": "404d3fbb-ee34-4757-8edd-1c5e0bc85ab5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gun_enemy",
    "eventList": [
        {
            "id": "c948fe4a-97f5-48a4-877e-a10f0e75a1a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "404d3fbb-ee34-4757-8edd-1c5e0bc85ab5"
        },
        {
            "id": "5ad7a45c-36cd-4531-9ae3-7938244c7e5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "404d3fbb-ee34-4757-8edd-1c5e0bc85ab5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1057713b-da8c-4405-9b1d-64608dc4b5f7",
    "visible": true
}
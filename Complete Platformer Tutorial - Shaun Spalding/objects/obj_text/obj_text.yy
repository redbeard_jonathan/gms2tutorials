{
    "id": "99159c06-19ad-48de-8532-04d60bbbd85a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_text",
    "eventList": [
        {
            "id": "335730fc-8f3c-4287-9f6f-f2de9fd077d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "99159c06-19ad-48de-8532-04d60bbbd85a"
        },
        {
            "id": "3cf7b0a1-ddd5-4637-9d47-f77953ba1ab1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "99159c06-19ad-48de-8532-04d60bbbd85a"
        },
        {
            "id": "d612ccdc-3855-4b55-9749-5835160fb9bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "99159c06-19ad-48de-8532-04d60bbbd85a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a4d2b3f5-d2ef-493e-8820-f5c3034b277a",
    "visible": true
}
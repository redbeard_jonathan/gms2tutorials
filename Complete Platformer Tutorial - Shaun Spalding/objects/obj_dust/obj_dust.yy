{
    "id": "da35796c-9763-4de8-9833-012684f4d8fa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_dust",
    "eventList": [
        {
            "id": "297936a3-7dc4-46d3-aa73-65fceab3e25d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "da35796c-9763-4de8-9833-012684f4d8fa"
        },
        {
            "id": "b23b92cb-7e9f-4124-b3ad-ebb37016817e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "da35796c-9763-4de8-9833-012684f4d8fa"
        },
        {
            "id": "8c70ed87-3b96-414b-ba0a-2653fb7fdb70",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "da35796c-9763-4de8-9833-012684f4d8fa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "780ec3c0-4695-4eba-8b63-71b11c2402fc",
    "visible": true
}